#!/bin/sh


SEND=/home/dstan/433Utils/RPi_utils/codesend
ENCODE=/home/dstan/scripts-perso/utils/build_relay.py
LOTUS=/home/dstan/scripts-perso/bin/lotus
TMPDIR=/tmp/relaystates

mkdir -p $TMPDIR
relay=$(echo "$1" | sed "s/[^0-9]//g")
onfile=$TMPDIR/$relay-on
if [ "$2" = "forceon" ]; then
  action=on
elif [ "$2" = "forceoff" ]; then
  action=off
elif [ -f $onfile ]; then
  action=off
else
  action=on
fi

send() {
  # $1 = numéro de relai, $2 = on or off
  if [ "$1" -eq 5 ]; then
    if [ "$2" = "on" ]; then
      $LOTUS f
    else
      $LOTUS o
    fi
  else
    $SEND $($ENCODE "$2" 4 "$1")
  fi
}

if [ "$action" = "off" ]; then
    send $relay off
    rm $onfile
else
    send $relay on
    touch $onfile
fi

