#!/usr/bin/ledit /usr/bin/python3
import serial, time
import dbus
from subprocess import call
import requests
import collections
import select
import socket
import sys
import os
import time

SERIALPORT = "/dev/ttyUSB0"
BAUDRATE = 9600
CONTROL=b'Control:'
SOCK_P='/var/run/user/%d/remote' % os.getuid()

ser = serial.Serial(SERIALPORT, BAUDRATE)
ser.bytesize = serial.EIGHTBITS #number of bits per bytes
ser.parity = serial.PARITY_NONE #set parity check: no parity
ser.stopbits = serial.STOPBITS_ONE #number of stop bits
#ser.timeout = None          #block read
#ser.timeout = 0             #non-block read
ser.timeout = 2              #timeout block read
ser.xonxoff = False     #disable software flow control
ser.rtscts = False     #disable hardware (RTS/CTS) flow control
ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
ser.writeTimeout = 0     #timeout for write

print('Starting Up Serial Monitor')

# Spotify iface
def get_interface(prog):
    bus = dbus.SessionBus()
    proxy = bus.get_object('org.mpris.MediaPlayer2.%s' % prog, '/org/mpris/MediaPlayer2')
    return dbus.Interface(proxy, dbus_interface='org.mpris.MediaPlayer2.Player')

def player_action(action):
    players = ['vlc', 'spotify']
    for pl in players:
        try:
            handle = get_interface(pl)
            getattr(handle, action)()
            return
        except dbus.exceptions.DBusException:
            pass
    
bus = dbus.SessionBus()
proxy = bus.get_object('org.gnome.ScreenSaver', '/org/gnome/ScreenSaver')
ss = dbus.Interface(proxy, dbus_interface='org.gnome.ScreenSaver')

# Volume control
def volume(p):
    call(["amixer", "-D", "pulse", "sset", "Master", p])

_states = collections.defaultdict(bool)
def light(p, state=None):
    """Light control"""
    p = str(p).encode()
    if state is None:
        state = not _states[p]
    ser.write((b'o' if state else b'f') + p + b';')
    ser.flush()
    _states[p] = state

def process_line(response):
    #print("Received %r" % response)
    #if response.startswith(b'Unkwon: 4294967295'):
    #    ss.Lock()
    #    return
    # TODO
    if response.startswith(b'Unkwon: 16621663'):
        call(['lotus', 'U'])
        return
    if response.startswith(b'Unkwon: 16625743'):
        call(['lotus', 'D'])
        return
    if response.startswith(b'Unkwon: 16607383'):
        call(['lotus', 'f'])
        return
    if response.startswith(b'Unkwon: 16623703'):
        call(['lotus', 'o'])
        return

    if response.startswith(CONTROL):
        key = response[len(CONTROL):].strip().lower()
        if key == b'pause':
            player_action('Pause')
        elif key == b'ppause':
            player_action('PlayPause')
        elif key.startswith(b"openuri:"):
            track=response[len(CONTROL)+9:].decode().strip()
            print(track)
            get_interface('spotify').OpenUri(track)
        elif key == b'prev':
            player_action('Previous')
        elif key == b'next':
            player_action('Next')
        elif key == b'vol+':
            volume('10%+')
        elif key == b'vol-':
            volume('10%-')
        elif key == b'stop':
            ss.Lock()
            ss.SetActive(True)
        elif key == b'enter':
            # TODO check that ping6 2a01:cb04:541:1400:be44:34ff:fe29:143b
            #                       ^^^ wikoo here
            ss.SetActive(False)
        else:
            state = None
            if key[0] in b"of":
                state = key[0] in b"o"
                key = key[1:]
            try:
                ikey = int(key)
            except ValueError:
                ikey = None
            if ikey and ikey in range(1,5):
                light(ikey, state)

# Socket unix specific
# Make sure the socket does not already exist
try:
    os.unlink(SOCK_P)
except OSError:
    if os.path.exists(SOCK_P):
        raise

server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
server.bind(SOCK_P)
server.listen(1)
clients = []

buffers = {}
def generic_readline(obj):
    global buffers
    if obj not in clients:
        yield obj.readline()
        return
    value = obj.recv(1024)
    if not value:
        raise ClientClosed()
    value = value.split(b'\n')
    value[0] = buffers.get(obj, b"") + value[0]
    buffers[obj] = value[-1]
    for line in value[:-1]:
        yield line + b'\n'

class ClientClosed(Exception):
    pass

if ser.isOpen():
    try:
        while True:
            todo, _, _ = select.select([server, ser, sys.stdin] + clients, [], [])
            for sock in todo:
                if sock is server:
                    connection, client_address = sock.accept()
                    connection.setblocking(0)
                    clients.append(connection)
                else:
                    try:
                        for line in generic_readline(sock):
                            if type(line) == str:
                                line = line.encode()
                            process_line(line)
                    except ClientClosed:
                        print("Removing client")
                        clients.remove(sock)
    except KeyboardInterrupt:
        print("Interupted by user... quitting.")

    except Exception as e:
        print("error communicating...: " + repr(e))
        raise
    finally:
        ser.close()
        sock.close()

else:
    print("cannot open serial port ")
