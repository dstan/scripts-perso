#!/usr/bin/ledit /usr/bin/python3

import subprocess
import sys
import dbus

# Volume control
XTE = subprocess.Popen(['/usr/bin/xte'], stdin=subprocess.PIPE)
def volume(p):
    #subprocess.call(["amixer", "-D", "default", "sset", "Master", p])
    assert(p[-1] in ['-', '+'])
    if p[-1] == '+':
        cmd = b'XF86AudioRaiseVolume'
    else:
        cmd = b'XF86AudioLowerVolume'
    XTE.stdin.write(b'key %b\n' % cmd)
    XTE.stdin.flush()

# Player control
def get_interface(prog):
    """Get dbus interface for given program"""
    bus = dbus.SessionBus()
    proxy = bus.get_object('org.mpris.MediaPlayer2.%s' % prog, '/org/mpris/MediaPlayer2')
    return dbus.Interface(proxy, dbus_interface='org.mpris.MediaPlayer2.Player')

def player_action(action):
    """Run an action on known players, in order.
    action in ['Pause', 'PlayPause', 'Next', 'Previous']"""
    players = ['vlc', 'spotify']
    for pl in players:
        try:
            handle = get_interface(pl)
            getattr(handle, action)()
            return
        except dbus.exceptions.DBusException:
            pass

# Screensaver definition
bus = dbus.SessionBus()
proxy = bus.get_object('org.gnome.ScreenSaver', '/org/gnome/ScreenSaver')
ss = dbus.Interface(proxy, dbus_interface='org.gnome.ScreenSaver')

def main_loop():
    
    proc = subprocess.Popen(['/usr/bin/ssh', 'minipudding.tudo.re', 
        '/usr/bin/irw'],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=sys.stderr,
    )
    for line in iter(proc.stdout.readline, ''):
        line = line.decode()
        code, count, key, remote = line.strip().split(' ')
        if key == 'KEY_VOLUMEUP':
            volume('6%+')
        elif key == 'KEY_VOLUMEDOWN':
            volume('6%-')
        elif key == 'KEY_PLAYPAUSE':
            player_action('PlayPause')
        elif key == 'KEY_PLAY':
            player_action('Play')
        elif key == 'KEY_PAUSE':
            player_action('Pause')
        elif key in ['KEY_RIGHT', 'SkipForward', 'KEY_NEXT']:
            player_action('Next')
        elif key in ['KEY_LEFT', 'Replay/SkipBackward', 'KEY_RESTART']:
            player_action('Previous')
        elif key == 'KEY_ENTER':
            ret, _ = subprocess.getstatusoutput(['ping cookie.tudo.re -c 4']) 
            if ret == 0:
                ss.SetActive(False)
            

if __name__ == '__main__':
    main_loop()
