#!/usr/bin/env python3

import sys

on = {'on': True, 'off': False}[sys.argv[1]]

channel = int(sys.argv[2])

port = int(sys.argv[3])

print("Sending %s to port %d of channel %d" % (('on' if on else 'off'), port, channel), file=sys.stderr)

code = 0b010101010101010101010101
code ^= 1<<(2*(4-channel)+16)
code ^= 1<<(2*(4-port)+8)
if not on:
    code ^= 1
print(code)
