#!/bin/bash

KEYID=90520CFDE846E7651A1B751FBC9BF8456E1C820B
REMOTE=origin
RBRANCH=master

#git fetch $REMOTE
if git verify-commit $REMOTE/$RBRANCH --raw 2>&1\
    | grep "^\[GNUPG:\] VALIDSIG.*$KEYID$"; then
    git merge --ff-only $REMOTE/$RBRANCH
else
    echo "GPG validation failed";
    exit 1
fi



