import paho.mqtt.client as mqtt
import subprocess
import json

DELTA=40
XDOTOOL='/usr/bin/xdotool'

def mouse_move(dir):
    """xdotool mousemove_relative 10 10"""
    args = {
        "left": [str(-DELTA), "0"],
        "up": ["0", str(-DELTA)],
        "down": ["0", str(DELTA)],
        "right": [str(DELTA), "0"],
    }[dir]

    subprocess.call([XDOTOOL, 'mousemove_relative', '--'] + args)

def mouse_click():
    print("click")
    subprocess.call([XDOTOOL, "click", "1"])

def volume(dir):
    if dir == 'up':
        arg = "XF86AudioRaiseVolume"
    else:
        arg = "XF86AudioLowerVolume"
    subprocess.call([XDOTOOL, "key", arg])
    
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("computer/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print("%r --> %r" % (msg.topic, msg.payload))
    data = None
    if msg.payload != b"":
        data = json.loads(msg.payload)
    print("%r" % data)
    if msg.topic.endswith('/move'):
        mouse_move(data['dir'])
    if msg.topic.endswith('/click'):
        mouse_click()
    if msg.topic.endswith('/volume'):
        volume(data['dir'])

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

import connect
connect.connect(client)
#client.tls_set()
#client.username_pw_set('root', 'xxxxxxxxxxxxx')
#client.connect("minipudding.tudo.re", 8883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
