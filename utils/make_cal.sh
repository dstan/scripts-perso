#!/bin/bash

ICS_LIST="https://www.mozilla.org/media/caldata/FrenchHolidays.ics https://www.mozilla.org/media/caldata/GermanHolidays.ics"
TARGET=~/scripts-perso/www.tudo.re/holidays.ics

echo "BEGIN:VCALENDAR" > $TARGET

(for url in $ICS_LIST; do
    wget -O - $url | grep -v "^\(BEGIN\|END\):VCALENDAR$"
done
)  >> $TARGET

echo "END:VCALENDAR"
