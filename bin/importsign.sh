#!/bin/sh

DIR=$HOME/mail/.INBOX.GPG/cur
DIR=$HOME/sshfs/cupcake/mail/.INBOX.GPG/cur
process() {
  gpg -d $1 | sed '/-----BEGIN PGP PUBLIC KEY BLOCK-----/!d; : proc {n; /-----END PGP PUBLIC KEY BLOCK-----/!b proc}' | gpg --import
  if echo "$1" | grep -vq ',S$'; then
    mv $1 ${1}S
  fi
}

if [ -z "$1" ]; then
  echo "No mail given, listing all";
  for x in $(ls "$DIR" | grep -v ",S$"); do
    process "$DIR/$x"
  done
else
  process $1
fi

