#!/usr/bin/env python3

import dbus
import sys
bus = dbus.SessionBus()
proxy = bus.get_object('org.roccat', '/org/roccat/Konepure')
interface=dbus.Interface(proxy, dbus_interface='org.roccat.Konepure')
def set_color(color):
    interface.GfxSetLedRgb(0, color)
    interface.GfxUpdate()


if __name__ == '__main__':
    if sys.argv[1:]:
        set_color(int(sys.argv[1], 16))
    else:
        print("Usage: ./led.py 0xaabbccdd")
        sys.exit(1)

