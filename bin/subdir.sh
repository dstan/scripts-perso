#!/bin/sh

TMP=/tmp/a
TARGET=./slides
cat > $TMP

name=$(sed "s/^.*label=\([0-9a-z\-]*\).*$/\1/;t; d" $TMP | head -n 1)

mv $TMP $TARGET/$name.tex
echo "\\input{$TARGET/$name.tex}"
