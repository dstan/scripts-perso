#!/bin/bash

DOMAINS="-d www.tudo.re -d cloud.tudo.re -d rc.tudo.re -d imap.tudo.re -d smtp.tudo.re -d my.tudo.re -d dstan.tudo.re"
DIR=/tmp/letsencrypt-auto

letsencrypt=/usr/bin/certbot
if [ ! -x "$letsencrypt" ]; then
    letsencrypt=/home/dstan/letsencrypt/letsencrypt-auto
fi
case $(/bin/hostname) in
  gladys)
  DOMAINS="$DOMAINS -d home.b2moo.fr"
  ;;

  cupcake)
  DOMAINS="$DOMAINS -d cupcake.tudo.re"
  ;;

  kugel)
  DOMAINS="$DOMAINS -d kugel.tudo.re"
  ;;
esac

mkdir -p $DIR
#$letsencrypt certonly -a webroot --webroot-path=$DIR $DOMAINS
#$letsencrypt renew -a webroot --webroot-path=$DIR $DOMAINS
$letsencrypt renew -a webroot --webroot-path=$DIR
