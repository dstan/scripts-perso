#!/bin/bash

DOMAINS=""
DIR=/tmp/letsencrypt-auto

letsencrypt=/usr/bin/certbot
case $(/bin/hostname) in
  kugel)
  DOMAINS="$DOMAINS -d re2o-dev.tudo.re -d kugel.tudo.re"
  ;;
  *)
  echo "Run on kugel only !"
  exit 1
  ;;
esac

mkdir -p $DIR
$letsencrypt certonly -a webroot --webroot-path=$DIR $DOMAINS
#$letsencrypt renew -a webroot --webroot-path=$DIR $DOMAINS
#$letsencrypt renew -a webroot --webroot-path=$DIR
