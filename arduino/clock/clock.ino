#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif


#define PIN 6
#define TOTAL 60
#define LED(X) (71-X)%TOTAL
#define REFRESH 10

#define PIEZO_PIN A0

Adafruit_NeoPixel strip = Adafruit_NeoPixel(TOTAL, PIN, NEO_GRBW + NEO_KHZ800);

const uint32_t c_ms = strip.Color(0, 0, 0, 120);
const uint32_t c_second = strip.Color(0, 10, 0, 0);
const uint32_t c_minute = strip.Color(0, 0, 127, 0);
const uint32_t c_hour = strip.Color(127, 0, 0, 0);
const uint32_t c_quad = strip.Color(0,0,120,0);
const uint32_t c_night = strip.Color(60,0,60,0);

void setup() {
  Serial.begin(9600);
  Serial.println("Hello");
  pinMode(PIEZO_PIN, INPUT);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

int hours = 0;
int minutes = 0;
int seconds = 0;
int ms = 0;
int last = 0;

void loop() {
  uint32_t col;
  unsigned long start = millis();
  unsigned long delta;
  if( Serial.available()){
     Serial.println("Reading hour...");
     hours = Serial.parseInt();
     minutes = Serial.parseInt();
     seconds = Serial.parseInt();
     Serial.println("Hour set to");
     Serial.print(hours); Serial.print(":");
     Serial.print(minutes); Serial.print(":");
     Serial.print(seconds); Serial.println(".");
  }

  for( int i = 0; i < 60; ++i) {
    col = 0;
    if( i < seconds)
      col = c_second;
      
    if( i%5 == 0) {
      if( hours >= 12)
        col = c_night;
      else
        col = c_quad;
    }
    if( i <= 5*(hours%12) + minutes/12)
      col |= c_hour;
    /*if( i < minutes)
      col |= c_minute;*/
    if( i*100 >= ms*6 && (i-1)*100 < ms*6)
      col = c_ms;
    if( (60-i)*100 <= ms*6 && (61-i)*100 > ms*6)
      col = c_ms;

    
    strip.setPixelColor(LED(i), col);
  }
  strip.show();
  
  if(last != seconds) {
    last = seconds;
    Serial.println(analogRead(PIEZO_PIN));
  }
  
  //update
  ms += REFRESH;
  seconds +=  ms/1000;
  ms %= 1000;
  if(seconds >= 60) {
     seconds = 0;
     minutes++;
     if(minutes > 60){
        minutes = 0;
        hours = (hours+1)%24;
     }
  }
  
  delta = millis() - start;
  
  if(delta > REFRESH) {
    Serial.print("We're late by ");
    Serial.println(((long) delta)-REFRESH);
  }
  else {
    delay(REFRESH-delta);
  }
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(LED(i), c);
    strip.show();
    delay(wait);
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(LED(i), Wheel((i+j) & 255));
    }
    strip.show();
    delay(wait);
  }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(LED(i), Wheel(((i * 256 / strip.numPixels()) + j) & 255));
    }
    strip.show();
    delay(wait);
  }
}

//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t wait) {
  for (int j=0; j<10; j++) {  //do 10 cycles of chasing
    for (int q=0; q < 3; q++) {
      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(LED(i+q), c);    //turn every third pixel on
      }
      strip.show();

      delay(wait);

      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(LED(i+q), 0);        //turn every third pixel off
      }
    }
  }
}

//Theatre-style crawling lights with rainbow effect
void theaterChaseRainbow(uint8_t wait) {
  for (int j=0; j < 256; j++) {     // cycle all 256 colors in the wheel
    for (int q=0; q < 3; q++) {
      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(LED(i+q), Wheel( (i+j) % 255));    //turn every third pixel on
      }
      strip.show();

      delay(wait);

      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(LED(i+q), 0);        //turn every third pixel off
      }
    }
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}
