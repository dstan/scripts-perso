#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif



#define PIEZO_PIN A0


void setup() {
  Serial.begin(9600);
  pinMode(PIEZO_PIN, INPUT);
}
int count = 0;

void loop() {
  int val = 0;
  val = analogRead(PIEZO_PIN);
  if( val > 30) {
    Serial.print("Triggered: ");
    Serial.println(val);
    delay(1000);
  }
  delay(10);
  if( count ++ > 100) {
     Serial.println("alive");
    count = 0; 
  }
}

