// Définition des codes boutons de ma télécommande, so far
#define BTN_UP 0xFDA05F
#define BTN_DOWN 0xFDB04F
#define BTN_PAUSE 0xFD807F
#define BTN_VOL_DOWN 0xFD00FF
#define BTN_VOL_UP 0xFD40BF
#define BTN_4 0xFD28D7
#define BTN_3 0xFD48B7
#define BTN_2 0xFD8877
#define BTN_1 0xFD08F7
#define BTN_LEFT 0xFD10EF
#define BTN_RIGHT 0xFD50AF
#define BTN_RETURN 0xFD708F
#define BTN_ENTER 0xFD906F
#define BTN_STOP 0xFD609F
#define BTN_SETUP 0xFD20DF
