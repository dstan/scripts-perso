
// Pin du moteur 
#define RF_M_CONT 12 //EN 3,4
#define RF_M_DRV1 11 //3A
#define RF_M_DRV2 10 //4A

// Pin relai ampoule
#define PIN_BULB 9

// Pin pour mesurer la position
#define PIN_POS A0

// Position hautes et basses
#define POS_HIGH 275
#define POS_LOW 105
#define BOUNCE 3

void setup()
{
  // Default light
  pinMode(PIN_BULB, OUTPUT);
  digitalWrite(PIN_BULB, HIGH);
   
  // Serial !
  Serial.begin(9600);

  // And the motor
  pinMode(RF_M_CONT, OUTPUT);
  pinMode(RF_M_DRV1, OUTPUT);
  pinMode(RF_M_DRV2, OUTPUT);
  digitalWrite(RF_M_CONT, LOW);
  digitalWrite(RF_M_DRV1, LOW);
  digitalWrite(RF_M_DRV2, LOW);
}

void move(bool going_up, int target=0, int timeout=8000) {
  int comp;
  int val;
  unsigned long deadline;

  deadline = millis() + timeout;

  if(target)
  {
      target = min(max(target, POS_LOW), POS_HIGH);
      going_up = get_position() < target;
  }
  else
  {
      target = going_up?POS_HIGH:POS_LOW;
  }

  Serial.print("Move:");
  Serial.print(going_up?"Up":"Down");
  Serial.print(",Target:");
  Serial.print(target);
  Serial.print(",Timeout:");
  Serial.println(timeout);

  if(going_up) {
    comp=1;
    digitalWrite(RF_M_DRV1, HIGH);
    digitalWrite(RF_M_DRV2, LOW);    
  } else {
    comp=-1;
    digitalWrite(RF_M_DRV1, LOW);
    digitalWrite(RF_M_DRV2, HIGH);    
  }
  digitalWrite(RF_M_CONT, HIGH);
  
  do {
    delay(1);
    val = get_position();
  }
  while(val *comp < target*comp && millis() < deadline);

  // End of move
  Serial.print("Done:");
  report_pos();

  digitalWrite(RF_M_CONT, LOW);
  digitalWrite(RF_M_DRV1, LOW);
  digitalWrite(RF_M_DRV2, LOW);
}

int get_position() {
  int value = 0;
  const int count = 10;
  for(int i=0; i < count; i++)
    value += analogRead(PIN_POS);
  return value/count;
}

void report_pos() {
  Serial.print("Report:"); Serial.println(get_position());
}

void loop() {
  int incomingByte = 0;
  if(Serial.available()) {
    incomingByte = Serial.read();
    // Debug:
    Serial.print("Input:"); Serial.println(incomingByte);
    switch(incomingByte) {
      case 'd':
        move(false, 0, 300);
        break;
      case 'u':
        move(true, 0, 500);
        break;
      case 'U':
        move(true, 1024);
        break;
      case 'D':
        move(false, 1);
        break;
      case 'm':
        move(false, Serial.parseInt());
      case 'r':
        report_pos();
        break;
      case 'o':
        digitalWrite(PIN_BULB, LOW);
        break;
      case 'f':
        digitalWrite(PIN_BULB, HIGH);
        break;
      default:
        Serial.println("Ignored");
    }
  }
}


