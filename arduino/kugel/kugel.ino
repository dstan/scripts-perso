/* Arduino directement connecté à kugel, pour communiquer
avec programme python coté port série*/

// Pin du recepteur Infrarouge
#define RECV_PIN 2

// Pin du rf 
#define RF_PIN 3

#include <IRLib.h>
#include <RCSwitch.h>


IRrecv My_Receiver(RECV_PIN);

RCSwitch mySwitch = RCSwitch();

// For ir-decoder
IRdecodeNEC My_Decoder;
unsigned int Buffer[RAWBUF];

// Pour les relais. Canal et numéro de relai (port) sur lequel envoyer une
// commande. On indique également s'il faut allumer ou éteindre
int todo_channel = 4;
int todo_port = 1;
boolean todo_on = true;
unsigned long code = 0;

void setup()
{
  Serial.begin(9600);
  delay(2000);
  while(!Serial);//delay for Leonardo
  My_Receiver.enableIRIn(); // Start the receiver
  My_Decoder.UseExtnBuf(Buffer);
  
  mySwitch.enableTransmit(RF_PIN);
}



void loop() {
  int val;
  int incomingByte = 0;
  if (My_Receiver.GetResults(&My_Decoder)) {
    //Restart the receiver so it can be capturing another code
    //while we are working on decoding this one.
    My_Receiver.resume();
    if ( My_Decoder.decode() )
      switch (My_Decoder.value) {
      case 0xFD807F:
        Serial.println("Control: PPause");
        break;
      case 0xFD00FF:
        Serial.println("Control: Vol-"); 
        break;
      case 0xFD40BF:
        Serial.println("Control: Vol+"); 
        break;
      case 0xFD08F7:
        Serial.println("Control: 1"); 
        break;
      case 0xFD8877:
        Serial.println("Control: 2"); 
        break;
      case 0xFD48B7:
        Serial.println("Control: 3"); 
        break;
      case 0xFD28D7:
        Serial.println("Control: 4"); 
        break;
      case 0xfd10ef:
        Serial.println("Control: Prev"); 
        break;
      case 0xfd50af:
        Serial.println("Control: Next"); 
        break;
      case 0xfd20df:
        Serial.println("Control: Setup");
        break;
      case 0xfd609f:
        Serial.println("Control: Stop");
        break;
      case 0xfd906f:
        Serial.println("Control: Enter");
        break;
      case 0xfd708f:
        Serial.println("Control: Return");
        break;
      default:
        Serial.print("Unkwon: ");
        Serial.println(My_Decoder.value);
      }
    //My_Decoder.DumpResults();
  }
  while(Serial.available()) {
    incomingByte = Serial.read();
    if(incomingByte == 'o')
      todo_on = true;
    else if(incomingByte == 'f')
      todo_on = false;
    else if(incomingByte >= '1' && incomingByte <= '4')
      todo_port = incomingByte - '0';
    else {
      code = 0b010101010101010101010101;
      code ^= (unsigned long) 1<<(2*(4-todo_channel)+16);
      code ^= 1<<(2*(4-todo_port)+8);
      if(!todo_on)
        code ^= 1;
      mySwitch.send(code, 24);
    }
  }
}


