<html>
 <head>
  <title>Daniel STAN</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

 </head>
 <body>
   <div style="float: right;">
    <img src="./myface.jpg" style="width: 207px; height: 207px;">
    <br/>
    Figure 1: My face
   </div>
   <h1>Daniel STAN</h1>
   <h2>This is not a placeholder; This is just an academic page</h2>
 <ul>
 <li><a href="http://arg.cs.uni-kl.de/gruppe/stan/">Old Academic page</a></li>
 <li><a href="https://www.lre.epita.fr/perso/daniel-stan/">My New Academic page</a></li>
 </ul>
 <h3>
   Contact me: 
 </h3>
 <ul>
    <li>daniel.stan@   {{domain name of the school I work for}}</li>
    <li>
    <a href="https://outlook.office365.com/calendar/published/1d063288162d4e81bd7b40d9c150879c@epita.fr/61cfbebe0e73419eb6cd6b76d428fda912100433509667759092/calendar.html">
    Meeting with me, here's my calendar</a>
    </li>
 </ul>
 </body>
</html>
