<html>
 <head>
  <title>tudo.re</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

 </head>
 <body>
   <h1>Tudor's webpage</h1>
 <ul>
 <li><a href="/daniel.stan/">Academic page</a></li>
 <li><a href="https://gitlab.crans.org/u/dstan">Gitlab profile at Crans</a></li>
 </ul>
 <h3>
   Contact me
 </h3>
 <ul>
   <li>Mail: $mynickname@tudo.re</li>
   <li><a href="dstan.gpg">PGP key</a></li>
 </ul>
 <h3>
   Some internal websites
 </h3>
 <ul>
   <li><a href="https://cloud.tudo.re/">My Own Nextcloud</a></li>
   <li><a href="https://rc.tudo.re/">Roundcube</a></li>
   <li><a href="https://nextbike.net/reservation/?maponly=1&language=en&city_ids=398&zoom=16&lat=49.4303&lng=7.7625">Grmmmbll bike grmmbl</a></li>
   <li><a href="https://cinemaallemand.tudo.re/">Blog du cinéma Allemand</a>
 </ul>
 </body>
</html>
