autocmd FileType latex,tex,md,markdown setlocal spell
set spelllang=en,fr
syntax on
set nu
set autoindent
set tabstop=4
set expandtab
set modeline
autocmd FileType make,markdown setlocal noexpandtab

highlight ColorColumn ctermbg=LightGrey guibg=LightGrey ctermfg=Black
execute "set colorcolumn=".join(range(81,335), ',')
if (&columns > 81) | set columns=81 | endif
autocmd VimResized * if (&columns > 81) | set columns=82 | endif

"match ErrorMsg '\%>80v.\+'
