#!/usr/bin/env python3
from xml.dom.minidom import parse, parseString
import sys
import requests
import time, os

PLACE_IDS=[2670, 2669, 2654, 2655,]
PATH="/tmp/nextbike.xml"
URL="https://nextbike.net/maps/nextbike-live.xml?&city=398"
REFRESH=5*60

if (not os.path.exists(PATH)) or time.time() - os.stat(PATH).st_mtime > REFRESH:
    r = requests.get(URL)
    with open(PATH,'w') as f:
        f.write(r.text)
    document = parseString(r.text)
else:
    document = parse(PATH)

places = document.getElementsByTagName('place')

def iterate():
    for place in places:
        try:
            number = int(place.getAttribute('number'))
        except:
            continue
        if number in PLACE_IDS:
            yield (number, place)
        

if 'config' in sys.argv[1:]:
    print("""graph_title Available bikes
graph_category environnement
graph_info number of bikes in my preferred rack stations in KL""")
    for (number, place) in iterate():
        print("place_%d.label %s" % (number, place.getAttribute('name')))
        print("place_%d.max %s" % (number, place.getAttribute('bike_racks')))
else:
    for (number, place) in iterate():
        print("place_%d.value %s" % (number, place.getAttribute('bikes')))
