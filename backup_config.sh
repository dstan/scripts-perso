
# command="/usr/bin/env ORIGIN=kugel /home/dstan/scripts-perso/backup-enc-recv ${SSH_ORIGINAL_COMMAND#* }",no-port-forwarding,no-X11-forwarding,no-pty

case "$(hostname)" in
  "ailesv")
    # Path to the backup fs
    FS_DIR=/home
    
    # Path to the backup directory (form FS_DIR)
    BACK_DIR=.snapshot
    
    # Extra filter for volumes (sed syntax)
    PATTERN=1d_.*
    
    # REMOTE command SSH
    REMOTE='ssh kugel.tudo.re -i /root/send_snap_home_kugel /root/scripts-perso/backup-recv'
    ;;
  "gigapudding")
    # Path to the backup fs
    FS_DIR=/mnt/backup
    
    # Path to the backup directory (form FS_DIR)
    BACK_DIR="$ORIGIN"
    
    # Extra filter for volumes (sed syntax)
    if [ "$ORIGIN" = "kugel" ]; then
        PATTERN=1d_.*
    elif [ "$ORIGIN" = "ailesv" ]; then
        PATTERN=1d_.*
    else
        echo '$ORIGIN not provided or invalid, aborting.'
        exit 10
    fi
    ;;
  "kugel")
    GPG_E="gpg -e -r daniel.stan@crans.org"
    if [ "$ORIGIN" = "kugel" ]; then
      # Path to the backup fs
      FS_DIR=/home
      
      # Path to the backup directory (form FS_DIR)
      BACK_DIR=.snapshot
      
      # Extra filter for volumes (sed syntax)
      PATTERN="1d_.*"
      
      # REMOTE command SSH
      REMOTE="ssh dstan@gigapudding.crans.org -i /root/backup_kugel_giga /home/dstan/scripts-perso/backup-enc-recv"
    else
      # Path to the backup fs
      FS_DIR=/mnt/backup
      
      # Path to the backup directory (form FS_DIR)
      BACK_DIR=ailesv
      
      # Extra filter for volumes (sed syntax)
      PATTERN=1d_.*
      
      # REMOTE command SSH
      REMOTE="ssh dstan@gigapudding.crans.org -i /root/backup_ailesv_giga /home/dstan/scripts-perso/backup-enc-recv"
    fi
    ;;
  *)
    echo "Unknown host !";
    exit 1
    ;;
esac

