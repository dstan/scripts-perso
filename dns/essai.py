# -*- coding: utf-8 -*-
import ovh
client = ovh.Client()

class UpdatableEntry(object):
    def __init__(self, client, t, fqdn, actual_value=None):
        self.client = client
        self.t = t
        words = fqdn.split('.')
        self.domain = '.'.join(words[-2:])
        self.name = '.'.join(words[:-2])
        self.rec_id = None
        self.actual_value = actual_value

        if self.t != 'dynHost':
            self.base_url = '/domain/zone/%s/record' % self.domain
        else:
            self.base_url = '/domain/zone/%s/dynHost/record' % self.domain

    def fill_request(self, ck):
        if self.rec_id is None:
            ck.add_recursive_rules(ovh.API_READ_WRITE, '/domain/zone/%s' % self.domain)
        else:
            ck.add_rule(ovh.API_READ_WRITE, '/domain/zone/%s/refresh' % self.domain)
            url = '%s/%d' % (self.base_url, self.rec_id)
            ck.add_rule(ovh.API_READ_WRITE, url)
    
    def find_id(self):
        for x in self.client.get(self.base_url):
            data = self.client.get('%s/%d' % (self.base_url, x))
            if self.t != 'dynHost' and data['fieldType'] != self.t:
                continue
            if data['subDomain'] != self.name:
                continue
            self.rec_id = x
            return
        raise KeyError('%s (%s).(%s)' % (self.t, self.name, self.domain))
 
    def update(self):
        if self.rec_id is None:
            self.find_id()
        url = '%s/%d' % (self.base_url, self.rec_id)
        if self.t != 'dynHost':
            key = 'target'
        else:
            key = 'ip'
        data = self.client.get(url)
        if data[key] != self.actual_value:
            print("%r to be updated (new value %s)" % (self, self.actual_value))
            if True:
                print("updating ...")
                self.client.put(url, **{key: self.actual_value})
                self.client.post('/domain/zone/%s/refresh' % self.domain)

    def __repr__(self):
        return self.name

updatable = [
    #UpdatableEntry(client, 'dynHost', 'kugel.tudo.re', u'86.246.91.178'),
    #UpdatableEntry(client, 'AAAA', 'kugel.tudo.re', u'2a01:cb04:541:1400:7a24:afff:fe31:dd36'),
    # true values
    UpdatableEntry(client, 'dynHost', 'kugel.tudo.re', u'86.246.91.178'),
    UpdatableEntry(client, 'AAAA', 'kugel.tudo.re', u'2a01:cb04:541:1400:7a24:afff:fe31:dd36'),
]

if False:
    print("Création d'une requête")
    ck = client.new_consumer_key_request()
    for entry in updatable:
        entry.fill_request(ck)

    print("""Please visit the following url: %(validationUrl)s
    consumerKey (later use): %(consumerKey)s""" % ck.request())
    raw_input()

print("Update now")
for entry in updatable:
    entry.update()

exit()

# Extrait 2:

import requests
import json

BASE_URL = 'http://192.168.1.1'
y = requests.post('%s/sysbus/Devices:get' % BASE_URL, data='{"parameters":{}}',
    headers={'Content-Type': 'application/x-sah-ws-1-call+json; charset=UTF-8'})

def get_pub_ip6(dev):
    for entry in dev.get('IPv6Address', []):
        if entry['Scope'] == 'global' and entry['Status'] == 'reachable':
            return entry['Address']
    return None

d = json.loads(y.text)['result']['status']

for x in d:
    print("%s -> %r" % (x['Name'], get_pub_ip6(x)))

# Extrait 1:
ck.add_recursive_rules(ovh.API_READ_ONLY, '/')
validation = ck.request()
validation
client.get('/me')
client.get('/')
client.get('/domain')
client.get('/domain/tudo.re')
client.get('/domain/zone')
client.get('/domain/zone/tudo.re')
client.get('/domain/zone/tudo.re/record')
client.get('/domain/zone/tudo.re/record/1346366029')
for x in client.get('/domain/zone/tudo.re/record/'):
    print(client.get('/domain/zone/tudo.re/record%d' % x)['subDomain'])
    
for x in client.get('/domain/zone/tudo.re/record/'):
    print(client.get('/domain/zone/tudo.re/record/%d' % x)['subDomain'])
    
for x in client.get('/domain/zone/tudo.re/record/'):
    print("%d -> %s" % client.get('/domain/zone/tudo.re/record/%d' % x)['subDomain'])
    
for x in client.get('/domain/zone/tudo.re/record/'):
    print("%d -> %s" % (x,client.get('/domain/zone/tudo.re/record/%d' % x)['subDomain']))
    
client.get('/domain/zone/tudo.re/record/1404164407')
client.get('/domain/zone/tudo.re/record/1404161856')
ck = client.new_consumer_key_request()
ck.add_recursive_rules(ovh.API_READ_WRITE, '/domain/zone/tudo.re/record/1404161856')
ck.request()
client.get('/domain/zone/tudo.re/record/1404161856')
client.get('/domain/zone/tudo.re/record/')
client.get('/domain/zone/tudo.re/record/1404161856')
client.put
client.put('/domain/zone/tudo.re/record/1404161856', target=u'2a01:cb04:541:1400:7a24:afff:fe31:dd36')
ck = client.new_consumer_key_request()
ck.add_recursive_rules(ovh.API_READ_WRITE, '/domain/zone/tudo.re')
ck.request
ck.request()
client.put('/domain/zone/tudo.re/record/1404161856', target=u'2a01:cb04:541:1400:7a24:afff:fe31:dd36')
client.post('/domain/zone/tudo.re/refresh')
client.post('/domain/zone/tudo.re/refresh')


